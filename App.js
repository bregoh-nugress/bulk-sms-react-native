import 'react-native-gesture-handler';
import { useState, useEffect, useMemo } from 'react';
import { StyleSheet, ActivityIndicator, View, Alert } from 'react-native';
import { NavigationContainer, DarkTheme } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Dashboard from './src/screens/DashboardScreen';
import MainMessages from './src/screens/messages/main';
import Profile from './src/screens/users/ProfileScreen';
import Support from './src/screens/SupportScreen';
import DrawerMenuContent from './src/components/DrawerMenuContents';
import RootStackScreen from './src/screens/RootStackScreen';
import { AuthContext } from './src/components/context';
import AsyncStorageLib from '@react-native-async-storage/async-storage';



const Drawer = createDrawerNavigator();

export default function App() {

	const [ token, setToken ] = useState(null);
	
	const authContext = useMemo(() => ({
		loginContext: async () => {
			const useToken = "abcd"
			await AsyncStorageLib.setItem("token", useToken)
			setToken(useToken)
		},
		logoutContext: async () => {
			await AsyncStorageLib.removeItem("token");
			setToken(null)
		}
	}));

	useEffect(async () => {
		const userToken = await AsyncStorageLib.getItem('token');

		if(userToken){
			setToken(userToken)
		}
	}, [])

	return (
		<AuthContext.Provider value={authContext}>
			<NavigationContainer>
				{
					token === null  ? <RootStackScreen /> : 
					(<Drawer.Navigator drawerContent={props => <DrawerMenuContent {...props} />} initialRouteName='Dashboard' screenOptions={{
						headerStyle: {
							backgroundColor: 'rgba(255, 193, 7, 1)'
						},
						headerTintColor: '#000',
						headerTitleStyle: {
							fontWeight: 'bold'
						}
					}}>
						<Drawer.Screen name="Dashboard" component={Dashboard} />
						<Drawer.Screen name="Messages" component={MainMessages} />
						<Drawer.Screen name="Profile" component={Profile} />
						<Drawer.Screen name="Support" component={Support} />
					</Drawer.Navigator> )
				}
			</NavigationContainer>
		</AuthContext.Provider>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
